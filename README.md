# Cara Deploy Aplikasi PHP ke AWS EBS #

Deployment dilakukan di sisi AWS melalui web interface, dan di lokal (laptop) untuk pembuatan source code

## Di sisi lokal (PHP)

1. Buat file `halo.php`

2. Masukkan file ke dalam zip

## Di sisi lokal (NodeJS)

1. Buat file `halo.js`

2. Masukkan file ke dalam zip

## Di Sisi AWS

1. Masuk ke menu Elastic Beanstalk

2. Create New Application

3. Select Platform : Pilih PHP 7.4

4. Upload file zip